# Programación Orientada a Objetos (OOP)

Es un [paradigma de programación](https://es.wikipedia.org/wiki/Programaci%C3%B3n_orientada_a_objetos) y una forma de organizar el código que puede facilitar la escritura de muchos tipos de aplicaciones combinando `propiedades` (variables) relacionadas y `métodos` (funciones) en `objetos` que contienen tanto comportamiento como estado.

Este tipo de programación utiliza `objetos` como elementos fundamentales en la construcción de la solución. Un `objeto` es una abstracción de algún hecho o ente del mundo real, con `atributos` que representan sus características o propiedades, y `métodos` que emulan su comportamiento o actividad. Todas las propiedades y métodos comunes a los objetos se encapsulan o agrupan en clases. Una `clase` es una plantilla, un prototipo para crear objetos; en general, se dice que cada objeto es una instancia o ejemplar de una clase.

<div align="center">
    <img src="/img/OOP_Objects.png" alt="OPP"
	title="Objects" width="500" height="350" />
</div>

## Clases

Para definir una clase se reemplaza la palabra reservada `def` con `class` y usamos la convención CamelCase para crear el nombre, seguido por `:`.

```python
class Beer:
  pass


```

## Instancias

Para crear un objeto:

- Se define una clase.
- Se instancía la clase usando `()`, de la misma manera como llamamos a una función en Python, de esta manera obtenemos una instancia de esta clase.
- Un objeto es una instancia de una clase.

```Python
class MyFirstClass:
  pass



#instancias de MyClass
obj_1 = MyFirstClass()
obj_2 = MyFirstClass()

```

## Ejercicio - Creando objetos en Python

Crea dos objetos para `dog`, `people`, `animal` y `book`.

```python
"""dog class"""



#dog objects



"""people class"""



#people objects


...


```